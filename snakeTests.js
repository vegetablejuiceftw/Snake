
 // Fred
function SnakeTests () {
    
    this.snake 		= new Snake( [0,0], [0,-1] );
    this.fail       = 0
    this.success    = 0
 
    this.runTests = function() {
        this.testInitialBody();
        this.testMoveBody();
        this.testEat();
        this.testGrow();
        this.testDrawSnake();
        console.log("Snake tests :: success: "+this.success+" failed: "+this.fail);
    };
    // Hendrik
    this.testInitialBody = function() {
        this.assertEqual(this.snake.body,"0,0,0,-1");
    }
    // Martin
    this.testMoveBody = function() {
        this.snake.move([0,1]);
        this.snake.move([0,1]);
        this.snake.move([1,0]);
        this.snake.move([0,1]);
        this.assertEqual(this.snake.body,"1,3,1,2") ;
    }
    // Fred
    this.testGrow = function() {
        this.snake.move([0,1]);
        this.snake.move([0,1]);
        this.assertEqual(this.snake.body,"1,5,1,4,1,3");        
        
    }
    // Hendrik
    this.testEat = function() {
        this.snake.eat();
        this.assertEqual( this.snake.digestion, "1,3");
        
    }
    // Fred
    this.testDrawSnake = function() {
        this.snake.move([1,0]);
        this.assertEqual( this.snake.drawShapes() , "0.8,4.8,1.4,0.4,0.8,3.8,0.4,1.4");
    }

    this.assertEqual = function(test, expected){
        if ( test.toString() == expected ){
            this.success+=1;
            return true;
        }
        else{
            console.log("input was:'"+test+"' and expecte:'"+expected+"'")
            this.fail += 1;
            return false;
        }
    }
  
}


test =  new SnakeTests();
test.runTests();