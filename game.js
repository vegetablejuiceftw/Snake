//GAME VARIABLES
var canvas;
var ctx;
var gameTimeout;
var animationFrame;
var running = true; 
var gameStart = false;

//playfield
var pixelRatio = 32;
var gameWidth = pixelRatio * 32;
var gameHeight = pixelRatio * 32;

// game tempo
var fps = 60;
var lastTic = 0; // init in gameInit 
//snake
var snake;

//key presses
var moveQuery = [];

// collision items
var collisionMap = [];

window.onload = function() {
	gameInit();
};

function gameInit() {
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");
	x = gameWidth / 2 / 32;
	y = gameWidth / 2 / 32;
	
	snake = new Snake([x - 1, y], [x, y - 1]);
	
	draw();
	moveQuery.push([1, 0]);

	generateWalls();
	
	lastTic = getTime();
	gameStart = false; // move later to reset variables
	gameLoop();
}

function gameLoop() {
	gameTimeout = setTimeout(function() {
		
		inputHandler();		
		animationFrame = requestAnimationFrame(gameLoop);
		
		if (gameStart){
			if (running) {
				update();
				draw();
			}
			else { 
				drawGameMessage("PAUSED");
			}
		}
		else {
			drawGameMessage("Press ENTER to begin");
		}

		
	}, 1000 / fps);
}
function getTime(){
	return Math.floor(new Date().getTime() / 100);
}
//updating gameState for drawing
function update() { // bad name
	if (moveQuery.length){
		var currentDir = moveQuery.shift();
		if (currentDir.toString() != snake.lastDirection.toString()){
			snake.move(currentDir);
			// console.log(currentDir,snake.lastDirection);
		}
			
	}
	else{
		if (lastTic != getTime()){
			lastTic = getTime();
			snake.move();	
		}
		
	}
	hasCollided();	// or wrong place?
}

//draws game
function draw() {
	ctx.clearRect(0, 0, gameWidth, gameHeight);
	//drawGrid();	
	drawSnake(); 
}

//gamefield drawing logcis
function drawGrid() {
	ctx.lineWidth = "1";
	ctx.strokeStyle = "#616161";
	for (i = 0; i <= gameWidth; i += pixelRatio) {
		for (j = 0; j <= gameHeight; j += pixelRatio) {
			ctx.strokeRect(i, j, pixelRatio, pixelRatio);
		}
	} 
}

//snake drawing logics
function drawSnake() {
	ctx.beginPath();
	ctx.fillStyle= "#689f38";
	var drawList = snake.drawShapes();
	for (var i = 0; i < drawList.length; i++) {
		var args = drawList[i];
		ctx.fillRect(args[0] * pixelRatio, args[1] * pixelRatio,
		args[2] * pixelRatio, args[3] * pixelRatio);	
	};
	ctx.fillStyle= "black";
}


function inputHandler() {
	var keyEvents = pollKeyEvents();
	var keyVectors = {
		"INPUT_UP": 	[0,- 1],
		"INPUT_RIGHT": 	[1, 0],
		"INPUT_DOWN": 	[0, 1],
		"INPUT_LEFT": 	[-1, 0]
	}
	for (var i = keyEvents.length - 1; i >= 0; i--) {
		var keyEvent = keyEvents[i];
		console.log(keyEvent);
		if (keyEvent == "INPUT_PAUSE"){
			pause();			
		}
		else if (keyEvent == "INPUT_ENTER"){
			console.log("start");
			gameStart=true;			
		}
		else if (running) {
			if (keyEvent == "INPUT_EAT"){
				snake.eat();			
			}
			else {
				var movementVector = keyVectors[keyEvent];
				if (movementVector) moveQuery.push(movementVector);
			}
		}
	};
}


//pauses the game loop
function pause() {
	running = !running;
}

function generateWalls() {
	if (!Object.keys(collisionMap).length ) {
		for (x = 0; x <= gameWidth / pixelRatio; x += gameWidth / pixelRatio) {
			for (y = 0; y <= gameHeight / pixelRatio; y += 1) {
				var block = [x, y];
				collisionMap[block] = block;
			}
		} 
		for (y = 0; y <= gameWidth / pixelRatio; y += gameWidth / pixelRatio) {
			for (x = 0; x <= gameWidth/pixelRatio; x += 1) {
				var block = [x, y];
				collisionMap[block] = block;		
			}		
		} 	
	}
	//for (var key in collisionMap) {
		//var block = collisionMap[key];		
		//ctx.fillRect(block[0] * pixelRatio - pixelRatio / 2, block[1] * pixelRatio - pixelRatio / 2, 
		//pixelRatio, pixelRatio);	
	//};
}

function drawGameMessage(message) {
	ctx.fillStyle = "#212121";
	ctx.font="40px arial, sans-serif";
	ctx.textAlign = "center";
	ctx.fillText(message, gameWidth / 2, gameHeight / 2);
}

function hasCollided () {
	var collisions = snake.getCollisions(collisionMap);
	if (collisions.length) reset();

	// collisions with itself
	collisions = snake.getSelfCollisions();
	if (collisions.length) reset();
}

function closeGameLoop() {
	clearTimeout(gameTimeout);
	cancelAnimationFrame(animationFrame);
}

function resetVariables() {
	moveQuery = [];
	collisionMap = [];
	running = true;
	gameStart = false;
}

function reset() {
	closeGameLoop();
	resetVariables();
	gameInit();
	//death animation here
	// var timeout = setTimeout(function() {
	// 	gameInit();
	// }, 100); //how long death animation will last
}
